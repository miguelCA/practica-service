package com.practica.users.practicaservice.model.repository;

import com.practica.users.practicaservice.model.domain.Estudiante;
import com.practica.users.practicaservice.model.domain.Matricula;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @autor miguel Corma
 */
public interface MatriculaRepository extends JpaRepository<Matricula,Long> {

}
