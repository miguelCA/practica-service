package com.practica.users.practicaservice.controller;

import com.practica.users.practicaapi.input.EstudianteInput;
import com.practica.users.practicaservice.model.Test;
import com.practica.users.practicaservice.model.domain.Estudiante;
import com.practica.users.practicaservice.model.repository.EstudianteRepository;
import com.practica.users.practicaservice.service.EstudianteCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @autor miguel Corma
 */
@RequestMapping(value ="/estudiante=>")
@RestController
@RequestScope

@Api(tags = "crear-Estudiante", description = "Student")
public class EstudianteController {

    @Autowired
    private Test test;

    @Autowired
    private EstudianteCreateService estudianteCreateService;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public  void probarTest(){

        test.print();
    }

    @ApiOperation(value = "TodoDeEstudiante")
    @ApiResponses(
            {
                    @ApiResponse(code = 201, message = "bien")
            }
    )
    @RequestMapping(method = RequestMethod.POST)
    public Estudiante crearEstudiante (@RequestBody EstudianteInput input){
        estudianteCreateService.setEstudianteInput(input);
        estudianteCreateService.execute();

        return estudianteCreateService.getEstudiante();
    }


    @RequestMapping(value = "/{idEstudiante}",method = RequestMethod.GET )
    public Estudiante bucarEstudianteId(@PathVariable(value = "idEstudiante") Long idEstudiante){
        return estudianteRepository.findByIdEstudiante(idEstudiante);
    }

}
