package com.practica.users.practicaservice.service;

import com.practica.users.practicaapi.input.EstudianteInput;
import com.practica.users.practicaservice.model.domain.Estudiante;
import com.practica.users.practicaservice.model.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor miguel Corma
 */
@Service
public class EstudianteCreateService {

    private EstudianteInput estudianteInput;
    @Autowired
    private EstudianteRepository estudianteRepository;

    private Estudiante estudiante;

    public  void  execute(){
        estudiante=crearEstudiante();
    }

    private Estudiante crearEstudiante() {
        Estudiante estudiante=new Estudiante();
        estudiante.setNombre(estudianteInput.getNombreEstudiante());
        estudiante.setApellidos(estudianteInput.getApellidoEstudiante());
        estudiante.setDireccion(estudianteInput.getDireccionEstudiante());

        estudianteRepository.save(estudiante);

        return estudiante;
    }

    public void setEstudianteInput(EstudianteInput estudianteInput) {
        this.estudianteInput = estudianteInput;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }
}
