package com.practica.users.practicaservice.service;

import com.practica.users.practicaapi.input.EstudianteInput;
import com.practica.users.practicaapi.input.MatriculaInput;
import com.practica.users.practicaservice.model.domain.Estudiante;
import com.practica.users.practicaservice.model.domain.Matricula;
import com.practica.users.practicaservice.model.repository.EstudianteRepository;
import com.practica.users.practicaservice.model.repository.MatriculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor miguel Corma
 */
@Service
public class
MatriculaCreateService {
    private Matricula matricula;

    private MatriculaInput matriculaInput;

    @Autowired
    private MatriculaRepository matriculaRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;


    public Estudiante devolverEstudiante(Long idEstudiante){
        return estudianteRepository.findByIdEstudiante(idEstudiante);
    }
    public Matricula crearMatricula(Long idEst, MatriculaInput matriculaInput){
        Matricula matricula=new Matricula();
        matricula.setAno(matriculaInput.getAno());
        matricula.setNivel(matriculaInput.getNivel());
        matricula.setEstudiante(devolverEstudiante(idEst));
        matriculaRepository.save(matricula);
        return matricula;
    }

}
