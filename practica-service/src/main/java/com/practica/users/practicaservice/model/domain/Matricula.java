package com.practica.users.practicaservice.model.domain;

import javax.persistence.*;

@Entity
public class Matricula {

    @Id
    @Column(name = "idMatricula",nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long idMatricula;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idEstudiante",referencedColumnName = "idEstudiante")
    private Estudiante estudiante;

    @Column(name="anio",length = 50,nullable = false)
    private int ano;

    @Column(name="nivel",nullable = false)
    private int nivel;

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Long getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(Long idMatricula) {
        this.idMatricula = idMatricula;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }


}
