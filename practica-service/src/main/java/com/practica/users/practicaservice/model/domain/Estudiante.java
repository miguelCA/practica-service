package com.practica.users.practicaservice.model.domain;

import javax.persistence.*;

@Entity
@Table(name = "estudiante")
public class Estudiante {
    @Id
    @Column(name="idEstudiante",nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private  Long idEstudiante;

    @Column(name="nombre",length = 50,nullable = false)
    private String nombre;

    @Column(name="apelidos",length = 50,nullable = false)
    private String apellidos;

    @Column(name = "direccion",length = 50,nullable = false)
    private String direccion;

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
