package com.practica.users.practicaservice.model.repository;

import com.practica.users.practicaservice.model.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudianteRepository  extends JpaRepository <Estudiante,Long>
{
           Estudiante findByIdEstudiante(Long id);
}
