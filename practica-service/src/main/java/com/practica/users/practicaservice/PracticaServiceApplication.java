package com.practica.users.practicaservice;

import com.practica.users.practicaservice.model.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
public class PracticaServiceApplication {

    public static void main(String[] args) {

        ApplicationContext context= SpringApplication.run(PracticaServiceApplication.class, args);
        Test test=(Test) context.getBean("test1");
        test.print();
    }

}

