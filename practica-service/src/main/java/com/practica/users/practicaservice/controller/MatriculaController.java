package com.practica.users.practicaservice.controller;

import com.practica.users.practicaapi.input.MatriculaInput;
import com.practica.users.practicaservice.model.domain.Estudiante;
import com.practica.users.practicaservice.model.domain.Matricula;
import com.practica.users.practicaservice.model.repository.MatriculaRepository;
import com.practica.users.practicaservice.service.MatriculaCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @autor miguel Corma
 */
@RequestMapping(value ="/Matricula")
@RestController
@RequestScope

@Api(tags = "Crear-Matricula",value = "matricula" +
        "")
public class MatriculaController {

    @Autowired
    private MatriculaRepository matriculaRepository;

    @Autowired
    private MatriculaCreateService matriculaCreateService;

    @ApiOperation(value = "buscarEstu")
    @ApiResponses(
            {
                    @ApiResponse(code = 201, message = "bien")
            }
    )
    @RequestMapping(value = "/{idEstudiante}", method = RequestMethod.GET)
    public Estudiante buscarEstudinate(@PathVariable("idEstudiante") Long id ){
        return matriculaCreateService.devolverEstudiante(id);
    }
    @ApiOperation(value = "crearMatricula")
    @ApiResponses(
            {
                    @ApiResponse(code = 201, message = "bien")
            }
    )
    @RequestMapping(value = "/{idEstudiante}",method = RequestMethod.POST)
    public Matricula crearMatricula(@PathVariable("idEstudiante") Long id,@RequestBody MatriculaInput matriculaInput){

        return matriculaCreateService.crearMatricula(id,matriculaInput);

    }

}
