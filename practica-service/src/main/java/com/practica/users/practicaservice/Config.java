package com.practica.users.practicaservice;

import com.practica.users.practicaservice.model.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean(name="test1")
    public Test test() {
        return new Test();
    }

}
