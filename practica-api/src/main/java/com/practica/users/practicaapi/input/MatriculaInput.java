package com.practica.users.practicaapi.input;

/**
 * @autor miguel Corma
 */
public class MatriculaInput {

    private int ano;

    private int nivel;

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
}
